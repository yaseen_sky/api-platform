var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}));

router.route('/')
	.get(function(req, res, next) {
        //retrieve all apps from Monogo
        mongoose.model('PaymentHistory').find({}, function (err, installs) {
              if (err) {
                  return console.error(err);
              } else {
                  //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
                  res.format({
                    html: function(){
                        res.json(installs);
                    },
                    json: function(){
                        res.json(installs);
                    }
                });
              }     
        });
    });
	
router.route('/:username')
  .get(function(req, res) {
    mongoose.model('PaymentHistory').find({ username: req.params.username}, function (err, appRevenues) {
      if (err) {
        console.log('GET Error: There was a problem retrieving: ' + err);
      } else {
			mongoose.model('PaymentHeader').find({ username: req.params.username}, function (err, paymentHeader) {
			  if (err) {
				console.log('GET Error: There was a problem retrieving: ' + err);
			  } else {
				  console.log(paymentHeader);
				res.format({
					  html: function(){
						   res.render('revenues', {
							'appRevenues' : appRevenues,
							'paymentHeader' : paymentHeader[0]
						  });
					  },
					  json: function(){
						  res.json(appRevenues);
					  }
					});
			}})
      }
    });
  })
.post(function (req, res) {
		 mongoose.model('PaymentHistory').create({
			  username: req.params.username,
			  date: req.body.date,
			  amount: req.body.amount
			}, function (err, app) {
              if (err) {
                  res.send("There was a problem adding the information to the database."+err);
              } else {
                  res.format({
                    html: function(){
                    },
                    //JSON response will show the newly created user
                    json: function(){
                        res.json(app);
                    }
                });
			  }
        })
	 });  
	/*
router.route('/:username/edit')
	.get(function(req, res) {
	    //search for the PaymentHistory within Mongo
	    mongoose.model('PaymentHistory').find({ username: req.params.username}, function (err, app) {
	        if (err) {
	            console.log('GET Error: There was a problem retrieving: ' + err);
	        } else {
	            //Return the user
	            console.log('GET Retrieving username: ' + app);
             
	            res.format({
	                html: function(){
	                       res.json(app);
	                 },
	                 //JSON response will return the JSON output
	                json: function(){
	                       res.json(app);
	                 }
	            });
	        }
	    });
})
.put(function(req, res) {
	mongoose.model('PaymentHistory').find({ username: req.params.username}, function (err, app) {
	        //update it
			console.log(app);
	        app[0].update({ 
				$push: {"payments": {
					"date": req.body.date,
					"amount": req.body.amount
					}
				}},
	        function (err, app) {
	          if (err) {
	              res.send("There was a problem updating the information to the database: " + err);
	          } 
	          else {
				  //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
				  res.format({
					  html: function(){
						   res.json(app);
					 },
					 //JSON responds showing the updated values
					json: function(){
						   res.json(app);
					 }
				  });
	           }
	        })
	    });
	});
	*/
	module.exports = router;
