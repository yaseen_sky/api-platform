var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}));

router.route('/')
	.get(function(req, res, next) {
        //retrieve all users from Monogo
        mongoose.model('User').find({}, function (err, users) {
              if (err) {
                  return console.error(err);
              } else {
                  //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
                  res.format({
                      //HTML response will render the index.jade file in the views/users folder. We are also setting "users" to be an accessible variable in our jade view
                    html: function(){
                        res.json(users);
                    },
                    //JSON response will show all users in JSON format
                    json: function(){
                        res.json(users);
                    }
                });
              }     
        });
    })
    .post(function (req, res) {
		var uname = req.body.username;		
		 mongoose.model('User').create({
			  name: uname,
			  username: uname,
			  password: req.body.password
			}, function (err, user) {
              if (err) {
                  res.send("There was a problem adding the information to the database."+err);
              } else {
					mongoose.model('PaymentHeader').create({
						  username: uname
						}, function (err, user) {
							  if (err) {
								  res.send("There was a problem adding the information to the database."+err);
							  } else {
									res.format({
										html: function(){},
										//JSON response will show the newly created user
										json: function(){ res.json(user); }
									});
							  }
						});
			}}
	)});
	 
// route middleware to validate :id
router.param('username', function(req, res, next, username) {
    //find the ID in the Database
    mongoose.model('User').find({ username: username}, function (err, user) {
        //if it isn't found, we are going to repond with 404
        if (err) {
            console.log(username + ' was not found');
            res.status(404)
            var err = new Error('Not Found');
            err.status = 404;
            res.format({
                html: function(){
                    next(err);
                 },
                json: function(){
                    next(err);
                 }
            });
        //if it is found we continue on
        } else {
            //uncomment this next line if you want to see every JSON document response for every GET/PUT/DELETE call
          //  console.log(user);
            // once validation is done save the new item in the req
            req.username = username;
            // go to the next thing
			next(); 
        } 
    });
});	 

router.route('/:username')
  .get(function(req, res) {
	  
    mongoose.model('User').find({ username: req.username}, function (err, user) {
      if (err) {
        console.log('GET Error: There was a problem retrieving: ' + err);
      } else {
        console.log('GET Retrieving username: ' + user.username);
       
        res.format({
          html: function(){
              res.json(user);
          },
          json: function(){
              res.json(user);
          }
        });
      }
    });
  });

	 
//DELETE an user by username
router.route('/:username/edit')
	//GET the individual user by username
	.get(function(req, res) {
	    //search for the user within Mongo
	    mongoose.model('User').find({ username: req.username}, function (err, user) {
	        if (err) {
	            console.log('GET Error: There was a problem retrieving: ' + err);
	        } else {
	            //Return the user
	            console.log('GET Retrieving username: ' + user);
             
	            res.format({
	                //HTML response will render the 'edit.jade' template
	                html: function(){
	                       res.json(user);
	                 },
	                 //JSON response will return the JSON output
	                json: function(){
	                       res.json(user);
	                 }
	            });
	        }
	    });
	})
	.delete(function (req, res){
    //find user by username
		 mongoose.model('User').findOneAndRemove({ username: req.username}, function (err, user) {
			if (err) {
				return console.error(err);
			} else {
				//remove it from Mongo
				console.log('DELETE removing'+user);
				 res.format({
					  html: function(){
						  res.json(user);
					  },
					  json: function(){
						  res.json(user);
					  }
				});				
			}
		});
	});	 

module.exports = router;
