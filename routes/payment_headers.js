var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}));

router.route('/')
	.get(function(req, res, next) {
        //retrieve all apps from Monogo
        mongoose.model('PaymentHeader').find({}, function (err, pHeaders) {
              if (err) {
                  return console.error(err);
              } else {
                  //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
                  res.format({
                    html: function(){
                        res.json(pHeaders);
                    },
                    json: function(){
                        res.json(pHeaders);
                    }
                });
              }     
        });
    });
	
router.route('/:username')
  .get(function(req, res) {
    mongoose.model('PaymentHeader').find({ username: req.params.username}, function (err, pHeaders) {
      if (err) {
        console.log('GET Error: There was a problem retrieving: ' + err);
      } else {
			res.format({
					  html: function(){
						   res.json(pHeaders);
					  },
					  json: function(){
						  res.json(pHeaders);
					  }
					});
      }
    });
  });
  
  // route middleware to validate :id
router.param('username', function(req, res, next, username) {
    //find the ID in the Database
    mongoose.model('PaymentHeader').find({ username: username}, function (err, user) {
        //if it isn't found, we are going to repond with 404
        if (err) {
            console.log(username + ' was not found');
            res.status(404)
            var err = new Error('Not Found');
            err.status = 404;
            res.format({
                html: function(){
                    next(err);
                 },
                json: function(){
                    next(err);
                 }
            });
        //if it is found we continue on
        } else {
            //uncomment this next line if you want to see every JSON document response for every GET/PUT/DELETE call
          //  console.log(user);
            // once validation is done save the new item in the req
            req.username = username;
            // go to the next thing
			next(); 
        } 
    });
});	
  
  
router.route('/:username/edit')
	.get(function(req, res) {
	    //search for the PaymentHistory within Mongo
	    mongoose.model('PaymentHeader').find({ username: req.params.username}, function (err, app) {
	        if (err) {
	            console.log('GET Error: There was a problem retrieving: ' + err);
	        } else {
	            //Return the user
	            console.log('GET Retrieving username: ' + app);
             
	            res.format({
	                html: function(){
	                       res.json(app);
	                 },
	                 //JSON response will return the JSON output
	                json: function(){
	                       res.json(app);
	                 }
	            });
	        }
	    });
})
.put(function(req, res) {
	mongoose.model('PaymentHeader').find({ username: req.params.username}, function (err, pheader) {
	        //update it
			console.log(pheader);
	        pheader[0].update({ 
				 totalCash: req.body.totalCash,
				  clearedCash: req.body.clearedCash,
				  pendingCash: req.body.pendingCash,
				  nextPayment: req.body.nextPayment
				},
	        function (err, pheader) {
	          if (err) {
	              res.send("There was a problem updating the information to the database: " + err);
	          } 
	          else {
				  //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
				  res.format({
					  html: function(){
						   res.json(pheader);
					 },
					 //JSON responds showing the updated values
					json: function(){
						   res.json(pheader);
					 }
				  });
	           }
	        })
	    });
	});

	module.exports = router;
