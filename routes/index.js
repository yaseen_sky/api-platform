var express = require('express');
var router = express.Router();

router.use('/users', require('../routes/users'));
router.use('/apps', require('../routes/apps'));
router.use('/applinks', require('../routes/applinks'));
router.use('/stats', require('../routes/appinstalls'));
router.use('/payment_history', require('../routes/payment_history'));
router.use('/payment_headers', require('../routes/payment_headers'));


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;