var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}));

router.route('/')
	.get(function(req, res, next) {
        //retrieve all apps from Monogo
        mongoose.model('App').find({}, function (err, apps) {
              if (err) {
                  return console.error(err);
              } else {
                  //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
                  res.format({
                    html: function(){
                        res.json(apps);
                    },
                    json: function(){
                        res.json(apps);
                    }
                });
              }     
        });
    })
    .post(function (req, res) {
	//	console.log(req.body);	
		 mongoose.model('App').create({
			  title: req.body.title,
			  appId: req.body.appId,
			  reviews: req.body.reviews,
			  ratings: req.body.ratings,
			  heading: req.body.heading,
			  description: req.body.description,
			  screenshots: req.body.screenshots,
			  revenue_per_click: req.body.revenue_per_click,
			  revenue_per_download: req.body.revenue_per_download,
			  returnUrl: req.body.returnUrl,
			  profilePic: req.body.profilePic
			 }, 
			 function (err, app) {
              if (err) {
                  res.send("There was a problem adding the information to the database."+err);
              } else {
                  //app has been created
                  console.log('POST creating new app: ' + app);
                  res.format({
                      //HTML response will set the location and redirect back to the home page. You could also create a 'success' page if that's your thing
                    html: function(){
                        res.json(app);
                    },
                    //JSON response will show the newly created app
                    json: function(){
                        res.json(app);
                    }
                });
				}
        })
	 });
	 
// route middleware to validate :id
router.param('appId', function(req, res, next, appId) {
    //find the ID in the Database
    mongoose.model('App').find({ appId: appId}, function (err, app) {
        //if it isn't found, we are going to repond with 404
        if (err) {
            console.log(app + ' was not found');
            res.status(404)
            var err = new Error('Not Found');
            err.status = 404;
            res.format({
                html: function(){
                    next(err);
                 },
                json: function(){
                    next(err);
                 }
            });
        //if it is found we continue on
        } else {
            //uncomment this next line if you want to see every JSON document response for every GET/PUT/DELETE call
          //  console.log(user);
            // once validation is done save the new item in the req
            req.appId = appId;
            // go to the next thing
			next(); 
        } 
    });
});	 

router.route('/:appId')
  .get(function(req, res) {
	  
    mongoose.model('App').find({ appId: req.appId}, function (err, app) {
      if (err) {
        console.log('GET Error: There was a problem retrieving: ' + err);
      } else {
      //  console.log('GET Retrieving app: ' + app[0].appId);
       
        res.format({
          html: function(){
			  var appData = (app[0])?app[0]:'No such application exists';
               res.render('app', {
                'app' : appData
              });
          },
          json: function(){
              res.json(app);
          }
        });
      }
    });
  });

	 
router.route('/:appId/edit')
	.get(function(req, res) {
	    //search for the user within Mongo
	    mongoose.model('App').find({ appId: req.appId}, function (err, app) {
	        if (err) {
	            console.log('GET Error: There was a problem retrieving: ' + err);
	        } else {
	            //Return the user
	            console.log('GET Retrieving username: ' + app);
             
	            res.format({
	                html: function(){
	                       res.json(app);
	                 },
	                 //JSON response will return the JSON output
	                json: function(){
	                       res.json(app);
	                 }
	            });
	        }
	    });
	});	 

module.exports = router;
