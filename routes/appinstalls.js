var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}));

router.route('/')
	.get(function(req, res, next) {
        //retrieve all apps from Monogo
        mongoose.model('AppInstalls').find({}, function (err, installs) {
              if (err) {
                  return console.error(err);
              } else {
                  //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
                  res.format({
                    html: function(){
                        res.json(installs);
                    },
                    json: function(){
                        res.json(installs);
                    }
                });
              }     
        });
    })
	.post(function (req, res) {
		 console.log('yaseen');
	 });
	
router.route('/:username')
  .get(function(req, res) {
    mongoose.model('AppInstalls').find({ username: req.params.username}, function (err, appinsalls) {
      if (err) {
        console.log('GET Error: There was a problem retrieving: ' + err);
      } else {
        res.format({
          html: function(){
             // var appinsalls = (app[0])?app[0]:'No such application exists';
               res.render('stats', {
                'appinsalls' : appinsalls
              });
          },
          json: function(){
              res.json(app[0]);
          }
        });
      }
    });
  });	
	
router.route('/:username/:appId/edit')
	.get(function(req, res) {
		 console.log('yaseenGt');
	    //search for the applink within Mongo
	    mongoose.model('AppInstalls').find({ username: req.params.username, appId: req.params.appId}, function (err, app) {
	        if (err) {
	            console.log('GET Error: There was a problem retrieving: ' + err);
	        } else {
	            //Return the user
	            console.log('GET Retrieving username: ' + app);
             
	            res.format({
	                html: function(){
	                       res.json(app);
	                 },
	                 //JSON response will return the JSON output
	                json: function(){
	                       res.json(app);
	                 }
	            });
	        }
	    });
})
.put(function(req, res) {
	var clicks = req.body.clicks;
	var installs = req.body.installs;
	mongoose.model('AppInstalls').find({ username: req.params.username, appId: req.params.appId}, function (err, app) {
	        //update it
			var appClicks = (app[0].clicks);
			var appInstalls = (app[0].installs);
			console.log(appInstalls);
	        app[0].update(
	           { $push: {"clicks": clicks,"installs": installs}}
	        , function (err, app) {
	          if (err) {
	              res.send("There was a problem updating the information to the database: " + err);
	          } 
	          else {
	                  //HTML responds by going back to the page or you can be fancy and create a new view that shows a success page.
	                  res.format({
	                      html: function(){
	                           res.json(app);
	                     },
	                     //JSON responds showing the updated values
	                    json: function(){
	                           res.json(app);
	                     }
	                  });
	           }
	        })
	    });
	});
	
	module.exports = router;
