var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}));

router.route('/')
	.get(function(req, res, next) {
        //retrieve all apps from Monogo
        mongoose.model('AppLink').find({}, function (err, apps) {
              if (err) {
                  return console.error(err);
              } else {
                  //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
                  res.format({
                    html: function(){
                        res.json(apps);
                    },
                    json: function(){
                        res.json(apps);
                    }
                });
              }     
        });
    })
    .post(function (req, res) {
	    //	console.log(req.body);	
      	//	var linkId = generateLink({appId: req.body.appId, username: req.body.username});
    		 mongoose.model('AppLink').create(
           {
      			  appId: req.body.appId,
      			  username: req.body.username
      			}, 
    			 function (err, appInfo) {
				   if (err) {
						res.send("There was a problem adding the information to the database."+err);
				   } else {
                //Link has been created
    				    mongoose.model('AppInstalls').create(
						{
      					  appId: appInfo.appId,
      					  username: appInfo.username,
      					  linkId: appInfo.linkId
    					  },
      					 function (err, InstallInfo) {
      						  if (err) {
      							  res.send("There was a problem adding the information to the database."+err);
      						  } else {
      							    console.log('App Install Info: ' + InstallInfo);
									 res.json(appInfo);
      						  }
      					 }) //Creattion of appinstalls ends here
    			}
    	 });
    }) 
	 
// route middleware to validate :id
router.param('username', function(req, res, next, username) {
    //find the ID in the Database
    mongoose.model('AppLink').find({ username: username}, function (err, user) {
        //if it isn't found, we are going to repond with 404
        if (err) {
            console.log(username + ' was not found');
            res.status(404)
            var err = new Error('Not Found');
            err.status = 404;
            res.format({
                html: function(){
                    next(err);
                 },
                json: function(){
                    next(err);
                 }
            });
        //if it is found we continue on
        } else {
            //uncomment this next line if you want to see every JSON document response for every GET/PUT/DELETE call
          //  console.log(user);
            // once validation is done save the new item in the req
            req.username = username;
            // go to the next thing
			next(); 
        } 
    });
});	

router.route('/:appId')
  .get(function(req, res) {
    mongoose.model('AppLink').find({ username: req.body.username, appId: req.params.appId}, function (err, appLink) {
      if (err) {
        console.log('GET Error: There was a problem retrieving: ' + err);
      } else {
        res.format({
          html: function(){
              res.json(appLink);
          },
          json: function(){
              res.json(appLink);
          }
        });
      }
    });
  });
	 
	 var generateLink = function(data){
		var uname = data.username.substring(2);
		var appId = data.appId.substring(2);
		
		return	uname+appId+math;
	 }
	 
	 module.exports = router;
