var myApp = angular.module('RefenueApp', ['ui.bootstrap', 'ui.bootstrap.datetimepicker']);

myApp.controller('AppCtrl', [
	'$scope', 
	'$http', 
	function($scope, $http) {
		console.log("controller");
		$scope.addUser = function() {
		  $http.post('/users', $scope.user).success(function(response) {
				console.log('User added'+ response);
			});
		}
		
		$scope.deleteUser = function() {
			$http.post('users/'+$scope.user.deleteuser+'/edit', {'_method':'delete'}).success(function(response) {
				console.log('User deleted'+ response);
			}	
				);
		}
		
		$scope.genLink = function(){
			var appInfo = {
				 appId: "Shoaib555",
				  username: "yaseen"			
				  }
			$http.post('/applinks', appInfo).success(function(response) {
				console.log('Link added'+ response);
			}	
				);
		};
		
		$scope.updateInstalls = function(){
			var appInfo = {
				 clicks: $scope.dates.clicks.getTime(),
				  installs: $scope.dates.installs.getTime(),
				  '_method':'put'
				  }
			$http.post('stats/'+$scope.appinstall.uname+'/'+$scope.appinstall.appId+'/edit', appInfo).success(function(response) {
				console.log('Link added'+ response);
			}	
				);
		};
		
		$scope.updateHeaders = function(){
			var appInfo = {
				 username: $scope.uheaders.username,
				  totalCash: $scope.uheaders.clearedCash+$scope.uheaders.pendingCash,
				  clearedCash:$scope.uheaders.clearedCash,
				  pendingCash:$scope.uheaders.pendingCash,
				  nextPayment: $scope.dates.uheaders.getTime(),
				  '_method':'put'
				  }
			$http.post('payment_headers/'+$scope.uheaders.username+'/edit', appInfo).success(function(response) {
				console.log('status added'+ response);
			}	
				);
		};
		
		
		$scope.updatePayments = function(){
			var appInfo = {
				 date: $scope.dates.payment.getTime(),
				 amount: $scope.payments.amount
				  }
			console.log('appInfo: '+ appInfo);	  
			$http.post('payment_history/'+$scope.payments.uname, appInfo).success(function(response) {
				console.log('payment updated'+ response);
			}	
				);	
		}
		
		// dates related methods
		$scope.dates = {
		clicks: new Date('01 Mar 2015'),
		installs: new Date('10 Mar 2015')
	  };
	  
	  $scope.open = {
		clicks: false,
		installs: false
	 };
	  
	  // Disable weekend selection
	  $scope.disabled = function(date, mode) {
		return (mode === 'day' && (new Date().toDateString() == date.toDateString()));
	  };

	  $scope.dateOptions = {
		showWeeks: false,
		startingDay: 1
	  };
	  
	  $scope.timeOptions = {
		readonlyInput: true,
		showMeridian: false
	  };
	  
	  $scope.openCalendar = function(e, date) {
		  e.preventDefault();
		  e.stopPropagation();

		  $scope.open[date] = true;
	  };
	  
	  // watch clicks and installs to calculate difference
	  $scope.$watch(function() {
		return $scope.dates;
	  }, function() {
		if ($scope.dates.clicks && $scope.dates.installs) {
		  var diff = $scope.dates.clicks.getTime() - $scope.dates.installs.getTime();
		  $scope.dayRange = Math.round(Math.abs(diff/(1000*60*60*24)))
			
		 // $window.updateData($scope.dates.clicks.getTime(),$scope.dates.installs.getTime());
		
		} else {
		  $scope.dayRange = 'n/a';
		}
	  }, true);
	  
	  // date related methods ends here
		
	
		
		$scope.addApp = function() {
			var appInfo = {
				  title: $scope.app.title,
				  appId: $scope.app.title,
				  reviews: 65,
				  ratings: 3,
				  heading: $scope.app.title+" App - Trainer led Yoga & Meditation Video Classes for Beginners, Regulars and Experts",
				  description: [$scope.app.title+" is your Personal Yoga Guru.","Thousands of Sequences - never get bored!", "Programs adapt and change based on your performance.","Real videos of trained and certified yoga instructors."],
				  screenshots: ["http://0170a872775ece1a89a4-24c2aa177861ba0db81c82f9ef3d90ab.r95.cf1.rackcdn.com/yogatailor-app-yoga-3day-program.png", "http://0170a872775ece1a89a4-24c2aa177861ba0db81c82f9ef3d90ab.r95.cf1.rackcdn.com/CustomizedVideos.png", "http://0170a872775ece1a89a4-24c2aa177861ba0db81c82f9ef3d90ab.r95.cf1.rackcdn.com/yogatailor-mission-ipad-iphone.jpg"],
				  cash: 20,
				  returnUrl: "refenue.com/"+$scope.app.title,
				  profilePic: $scope.app.profilePic,
				  revenue_per_click: $scope.app.clickpay,
				  revenue_per_download: $scope.app.downloadpay
				  
			}
			$http.post('/apps', appInfo).success(function(response) {
				console.log('App added'+ response);
			}	
				);
		};
	}
	]);