
var myApp = angular.module('RefenueApp', ['ui.bootstrap', 'ui.bootstrap.datetimepicker']);

	
	myApp.controller('paymentsCtrl', ['$scope', '$sce', '$window', function($scope, $sce, $window) {
	 var drawTable = function(){
		var payments = $window.payments;
		var paymentsHTML = '';
		for (var key in payments) {
			var obj = payments[key];
			if(obj.date >= $scope.dates.date1.getTime() && obj.date <= $scope.dates.date2.getTime()){
				paymentsHTML = paymentsHTML + '<tr><td>'+new Date(obj.date).toDateString()+'</td><td>'+obj.amount+'</td></tr>';
			}
		}
		
		$scope.payments_tbody =  $sce.trustAsHtml(paymentsHTML);
	 }	
	 
	  $scope.dates = {
		date1: new Date('01 Aug 2015'),
		date2: new Date('10 Dec 2015')
	  };
	  
	  $scope.open = {
		date1: false,
		date2: false
	 };
	  
	  // Disable weekend selection
	  $scope.disabled = function(date, mode) {
		return (mode === 'day' && (new Date().toDateString() == date.toDateString()));
	  };

	  $scope.dateOptions = {
		showWeeks: false,
		startingDay: 1
	  };
	  
	  $scope.timeOptions = {
		readonlyInput: true,
		showMeridian: false
	  };
	  
	  $scope.openCalendar = function(e, date) {
		  e.preventDefault();
		  e.stopPropagation();

		  $scope.open[date] = true;
	  };
	  
	  // watch date4 and date5 to calculate difference
	  $scope.$watch(function($window) {
		return $scope.dates;
	  }, function() {
		  drawTable();
		/*if ($scope.dates.date4 && $scope.dates.date5) {
		  var diff = $scope.dates.date4.getTime() - $scope.dates.date5.getTime();
		  $scope.dayRange = Math.round(Math.abs(diff/(1000*60*60*24)))
			
		  $window.updateData($scope.dates.date4.getTime(),$scope.dates.date5.getTime());
		
		} else {
		  $scope.dayRange = 'n/a';
		}*/
	  }, true);
	  
	  drawTable();
	}]);