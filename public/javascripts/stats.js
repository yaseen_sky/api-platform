	var updateData = function(time1, time2){
		data = [
	  {
		"key": "Clicks",
		"values": [ [ time1 , 0] , [ time1 , 6] ,[ time1 , 1], [ time2 , 5] , [ time2 , 13] ]
	  },
	  {
		"key": "Installations",
		"values": [ [ time1 , 0] , [ time1 , 10] , [ time1 , 20] , [ time2 , 10] , [ time2 , 40] ]
	  }
	  
	]
	reDraw();
	
	};

	var app = angular.module('RefenueApp', ['ui.bootstrap', 'ui.bootstrap.datetimepicker']);

	app.controller('datePickerCtrl', ['$scope', '$window', function($scope, $window) {
	//	$window.updateData(1,2);
		
	  $scope.dates = {
		date4: new Date('01 Mar 2015'),
		date5: new Date('10 Mar 2015')
	  };
	  
	  $scope.open = {
		date4: false,
		date5: false
	 };
	  
	  // Disable weekend selection
	  $scope.disabled = function(date, mode) {
		return (mode === 'day' && (new Date().toDateString() == date.toDateString()));
	  };

	  $scope.dateOptions = {
		showWeeks: false,
		startingDay: 1
	  };
	  
	  $scope.timeOptions = {
		readonlyInput: true,
		showMeridian: false
	  };
	  
	  $scope.openCalendar = function(e, date) {
		  e.preventDefault();
		  e.stopPropagation();

		  $scope.open[date] = true;
	  };
	  
	  // watch date4 and date5 to calculate difference
	  $scope.$watch(function($window) {
		return $scope.dates;
	  }, function() {
		if ($scope.dates.date4 && $scope.dates.date5) {
		  var diff = $scope.dates.date4.getTime() - $scope.dates.date5.getTime();
		  $scope.dayRange = Math.round(Math.abs(diff/(1000*60*60*24)))
			
		  $window.drawGraph($scope.dates.date4.getTime(),$scope.dates.date5.getTime());
		
		} else {
		  $scope.dayRange = 'n/a';
		}
	  }, true);
	}]);

var chart;

var reDraw = function(){
	nv.addGraph(function() {
			chart = nv.models.cumulativeLineChart()
				.x(function(d) { return d[0] })
				//adjusting, 100% is 1.00, not 100 as it is in the data
				.y(function(d) { return d[1]})
				.color(d3.scale.category10().range())
				.useInteractiveGuideline(true);

	  chart.xAxis
		.tickFormat(function(d) {
		  return d3.time.format('%x')(new Date(d))
		});

	  chart.yAxis.tickFormat();

	  d3.select('#chart svg')
		.datum(data)
		.transition().duration(500)
		.call(chart)
		;

	  nv.utils.windowResize(chart.update);

	  return chart;
	});
}	

/* var data = [
  {
    "key": "Clicks",
    "values": [ [ 1025409600000 , 0] , [ 1028088000000 , 6] ,[ 1028088000000 , 1], [ 1030766400000 , 5] , [ 1033358400000 , 13] ]
  },
  {
    "key": "Installations",
    "values": [ [ 1025409600000 , 0] , [ 1028088000000 , 10] ,[ 1030766400000 , 20] , [ 1033358400000 , 10] , [ 1036040400000 , 40] ]
  }
  
] */

function sortdata(arr) {
    var a = [], b = [], prev;

    arr.sort();
    for ( var i = 0; i < arr.length; i++ ) {
        if ( arr[i] !== prev ) {
            a.push(arr[i]);
            b.push(1);
        } else {
            b[b.length-1]++;
        }
        prev = arr[i];
    }

    return [a, b];
}