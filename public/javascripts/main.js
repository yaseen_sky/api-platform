var myApp = angular.module('RefenueApp', ['ui.bootstrap']);

myApp.controller('RatingCtrl', function ($scope) {
  var rateValue = document.getElementById("rateValue").value;
  $scope.rate = rateValue;
  $scope.max = 5;
  $scope.isReadonly = false;

  $scope.hoveringOver = function(value) {
    $scope.overStar = value;
    $scope.percent = 100 * (value / $scope.max);
  };

  $scope.ratingStates = [
    {stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle'},
    {stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty'},
    {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
    {stateOn: 'glyphicon-heart'},
    {stateOff: 'glyphicon-off'}
  ];
});

myApp.controller('appLinkCtrl', [
	'$scope', 
	'$http', 
	'$window',
	function($scope, $http, $window) {
		if($window.username){
		  $http.get('/applinks/'+appId+'?username='+$window.username).success(function(response) {
				if(response.linkId){
					$scope.appLink = location.host+'/'+response.linkId;
					document.getElementById("appLinkDisplay").className='show';
				}else{
					document.getElementById("appLinkDisplay").className='hide';
					document.getElementById("appLinkG").className='show';
				}	
		  });
		}else{
			document.getElementById("appLinkDisplay").className='hide';
			document.getElementById("appLinkG").className='show';
		};
		
		
		$scope.genLink = function(){
			var appInfo = {
				 appId: $window.appId,
				  username: $window.username		
			  }
			$http.post('/applinks', appInfo).success(function(response) {
				console.log('Link added'+ response);
				$scope.appLink = location.host+'/'+response.linkId;
				document.getElementById("appLinkDisplay").className='show';
				document.getElementById("appLinkG").className='hide';
			});
		};
	}
	
	
]);
