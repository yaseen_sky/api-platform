var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var appSchema = new Schema({
  title: String,
  appId: String,
  reviews: Number,
  ratings: Number,
  heading: String,
  description: Array,
  screenshots: Array,
  revenue_per_click: Number,
  revenue_per_download: Number,
  returnUrl: String,
  profilePic: String,
  created_at: Date,
  updated_at: Date
});

appSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();
  
  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

mongoose.model('App', appSchema);