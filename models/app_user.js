var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var appUserSchema = new Schema({
  appId: String,
  username: String,
  created_at: Date,
  updated_at: Date
});

appUserSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();
  
  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

mongoose.model('AppUser', appUserSchema);