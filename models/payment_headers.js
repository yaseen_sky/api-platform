var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var paymentHeaderSchema = new Schema({
  username: String,
  bank: String,
  branch: String,
  accountNo: String,
  ifscCode: String,
  totalCash: Number,
  clearedCash: Number,
  pendingCash: Number,
  nextPayment: Number,
  created_at: Date,
  updated_at: Date
});

paymentHeaderSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();
  
  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

mongoose.model('PaymentHeader', paymentHeaderSchema);