var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/refenue');

require('../models/app');
require('../models/user');
require('../models/app_user');
require('../models/app_link');
require('../models/app_installs');
require('../models/payment_history');
require('../models/revenue');
require('../models/payment_headers');


/*
REF_USERS -- All subscribed users
REF_APPS -- All registered apps
REF_APP_LINKS -- links generated for each user 
REF_APP_INSTALLS -- Install and conversion information for each app/reference
REF_APP_PAYMENT_HEADERS, REF_APP_PAYMENT_LINES -- Payment history


middleware js
roles in users db
node js redis for app links 
app_users in another table

*/