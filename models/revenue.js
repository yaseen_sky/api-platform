var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var revenueSchema = new Schema({
  appId: String,
  username: String,
  username: String,
  date: Number,
  amount: Number
});


revenueSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();
  
  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

mongoose.model('Revenue', revenueSchema);