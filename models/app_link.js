var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var shortid = require('shortid');
var uniqueValidator = require('mongoose-unique-validator');

// create a schema
var linkSchema = new Schema({
  appId: String,
  username: String,
  active: Boolean,
  linkId: {
    type: String,
    unique: true,
    'default': shortid.generate
  },
  created_at: Date,
  updated_at: Date
});

linkSchema.plugin(uniqueValidator);

linkSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();
  
  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

mongoose.model('AppLink', linkSchema);