var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var paymentHistorySchema = new Schema({
  username: String,
  date: Number,
  amount: Number
});

mongoose.model('PaymentHistory', paymentHistorySchema);